package com.zzl.springboot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zzl.springboot.entity.Files;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Zzl
 * @since 2024-03-26
 */
public interface IFilesService extends IService<Files> {

    String uploadFile(MultipartFile file);
}
