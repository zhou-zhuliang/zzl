package com.zzl.springboot.service.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zzl.springboot.common.Constants;
import com.zzl.springboot.entity.Files;
import com.zzl.springboot.exception.ServiceException;
import com.zzl.springboot.mapper.FilesMapper;
import com.zzl.springboot.service.IFilesService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Zzl
 * @since 2024-03-26
 */
@Service
public class FilesServiceImpl extends ServiceImpl<FilesMapper, Files> implements IFilesService {

    @Value("${files.upload.path}")
    private String fileUploadPath;

    @Value("${server.ip}")
    private String serverIp;

    @Override
    public String uploadFile(MultipartFile file) {
        // 获取文件名
        String originalFilename = file.getOriginalFilename();
        // 获取文件大小 转为kb
        long size = file.getSize() / 1024;
        // 获取文件类型
        String type = FileUtil.extName(originalFilename);
        // 判断配置的文件目录是否存在，若不存在则创建一个新的目录
        File parentFile = new File(fileUploadPath);
        if (!parentFile.exists()) {
            parentFile.mkdir();
        }
        // 生成一个uuid，当作文件名
        String uuid = IdUtil.fastSimpleUUID();
        String fileUUID = uuid + StrUtil.DOT + type;
        // 编写文件全路径
        String filePath = fileUploadPath + fileUUID;
        File upFile = new File(filePath);
        String url;
        try {
            // 将文件保存到磁盘中
            file.transferTo(upFile);
            // 获取文件的md5，为了判断文件是否存在，存在就删掉，节省空间。
            String md5 = SecureUtil.md5(upFile);
            // 从数据库查询md5是否存在
            List<Files> filesList = list(new QueryWrapper<Files>().lambda().eq(Files::getMd5, md5));
            if (filesList != null && filesList.size() > 0) {
                // 如果有md5相同的文件，这个文件就不需要存了，直接获取之前存过的文件路径保存
                url = filesList.get(0).getUrl();
                // 由于文件已存在，所以删除刚才上传的重复文件
                upFile.delete();
            }else {
                url = "http://" +serverIp+ ":9090/files/" + fileUUID;
            }
            Files files = new Files();
            // 将文件信息保存到数据库中。
            files.setUrl(url);
            files.setName(originalFilename);
            files.setSize(size);
            files.setType(type);
            files.setMd5(md5);
            save(files);
            return files.getUrl();
        } catch (IOException e) {
            e.printStackTrace();
            throw  new ServiceException(Constants.CODE_500,"文件上传失败");
        } catch (Exception e) {
            e.printStackTrace();
            throw  new ServiceException(Constants.CODE_601,"系统错误");
        }

    }
}
