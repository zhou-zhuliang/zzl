package com.zzl.springboot.service.impl;

import com.zzl.springboot.entity.MavTotal;
import com.zzl.springboot.mapper.MavTotalMapper;
import com.zzl.springboot.service.IMavTotalService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Zzl
 * @since 2024-04-19
 */
@Service
public class MavTotalServiceImpl extends ServiceImpl<MavTotalMapper, MavTotal> implements IMavTotalService {

}
