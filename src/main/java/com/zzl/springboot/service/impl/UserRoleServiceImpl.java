package com.zzl.springboot.service.impl;

import com.zzl.springboot.entity.UserRole;
import com.zzl.springboot.mapper.UserRoleMapper;
import com.zzl.springboot.service.IUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Zzl
 * @since 2024-04-02
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements IUserRoleService {

}
