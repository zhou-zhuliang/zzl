package com.zzl.springboot.service.impl;

import com.zzl.springboot.entity.Role;
import com.zzl.springboot.mapper.RoleMapper;
import com.zzl.springboot.service.IRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Zzl
 * @since 2024-03-31
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

}
