package com.zzl.springboot.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.zzl.springboot.entity.OperLog;
import com.zzl.springboot.entity.OperLogEvent;
import com.zzl.springboot.mapper.OperLogMapper;
import com.zzl.springboot.service.IOperLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zzl.springboot.utils.AddressUtils;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Zzl
 * @since 2024-04-22
 */
@Service
public class OperLogServiceImpl extends ServiceImpl<OperLogMapper, OperLog> implements IOperLogService {

    /**
     * 操作日志记录
     *
     * @param operLogEvent 操作日志事件
     */
    @Async
    @EventListener
    public void recordOper(OperLogEvent operLogEvent) {
        OperLog operLog = BeanUtil.toBean(operLogEvent, OperLog.class);
        // 远程查询操作地点
        operLog.setOperLocation(AddressUtils.getRealAddressByIP(operLog.getOperIp()));
        operLog.setOperId(UUID.randomUUID().toString());
        save(operLog);
    }
}
