package com.zzl.springboot.service.impl;

import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zzl.springboot.entity.Logininfor;
import com.zzl.springboot.entity.LogininforEvent;
import com.zzl.springboot.mapper.LogininforMapper;
import com.zzl.springboot.service.ILogininforService;
import com.zzl.springboot.utils.AddressUtils;
import com.zzl.springboot.utils.ServletUtils;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Zzl
 * @since 2024-04-17
 */
@Service
public class LogininforServiceImpl extends ServiceImpl<LogininforMapper, Logininfor> implements ILogininforService {

    /**
     * 记录登录信息
     *
     * @param logininforEvent 登录事件
     */
    @Async
    @EventListener
    public void recordLogininfor(LogininforEvent logininforEvent) {
        HttpServletRequest request = logininforEvent.getRequest();
        final UserAgent userAgent = UserAgentUtil.parse(request.getHeader("User-Agent"));
        final String ip = ServletUtils.getClientIP(request);

        String address = AddressUtils.getRealAddressByIP(ip);
        // 获取客户端操作系统
        String os = userAgent.getOs().getName();
        // 获取客户端浏览器
        String browser = userAgent.getBrowser().getName();
        // 封装对象
        Logininfor logininfor = new Logininfor();
        logininfor.setInfoId(UUID.randomUUID().toString());
        logininfor.setUserName(logininforEvent.getUsername());
        logininfor.setLoginTime(new Date());
        logininfor.setIpaddr(ip);
        logininfor.setLoginLocation(address);
        logininfor.setBrowser(browser);
        logininfor.setOs(os);
        logininfor.setMsg(logininforEvent.getMessage());
        logininfor.setStatus(logininforEvent.getStatus());
        // 插入数据
        save(logininfor);
    }




}
