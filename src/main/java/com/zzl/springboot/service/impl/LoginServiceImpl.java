package com.zzl.springboot.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zzl.springboot.common.Constants;
import com.zzl.springboot.config.TokenUtil;
import com.zzl.springboot.controller.dto.PasswordDto;
import com.zzl.springboot.controller.dto.UserDto;
import com.zzl.springboot.entity.LogininforEvent;
import com.zzl.springboot.entity.User;
import com.zzl.springboot.exception.ServiceException;
import com.zzl.springboot.mapper.LoginMapper;
import com.zzl.springboot.service.IUserService;
import com.zzl.springboot.service.LoginService;
import com.zzl.springboot.utils.ServletUtils;
import com.zzl.springboot.utils.SpringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  登录服务实现类
 * </p>
 *
 * @author Zzl
 * @since 2024-03-21
 */
@Service
public class LoginServiceImpl extends ServiceImpl<LoginMapper,User> implements LoginService  {

    @Autowired
    private IUserService userService;


    @Override
    public UserDto login(UserDto userDto) {
        User one = getUserInfo(userDto);
        if(one != null && one.getPassword().equals(userDto.getPassword())){
            BeanUtil.copyProperties(one,userDto,true);//从one对象复制属性值到userDto对象但只有当one的属性值不为null时才进行复制
            userDto.setToken(TokenUtil.getToken(one.getId().toString()));//设置token
            recordLogininfor(userDto.getUsername(), "0", "登录成功");
            return userDto;
        }else {
            recordLogininfor(userDto.getUsername(), "1", "账号或密码错误");
            throw new ServiceException(Constants.CODE_600,"账号或密码错误");

        }
    }

    @Override
    public User register(UserDto userDto) {
        User one = getUserInfo(userDto);
        if (one == null){
            one = new User();
            BeanUtil.copyProperties(userDto,one,true);
            save(one);
            recordLogininfor(userDto.getUsername(), "0", "注册成功");
            return one;
        }else {
            throw new ServiceException(Constants.CODE_600,"用户名已存在");
        }

    }

    @Override
    public User editPw(PasswordDto passwordDto) {
        User user = TokenUtil.getUser();
        assert user != null;
        Integer stUserId = user.getId();
        User userById = userService.getById(stUserId);
        String newPassword = passwordDto.getNewPassword();
        String oldPassword = passwordDto.getOldPassword();
        if (!oldPassword.equals(userById.getPassword())) {
            throw new ServiceException(Constants.CODE_400, "原密码错误");
        } else if (userById.getPassword().equals(newPassword)) {
            throw new ServiceException(Constants.CODE_400, "新密码不能和旧密码相同");
        } else if (StringUtils.isBlank(newPassword)) {
            throw new ServiceException(Constants.CODE_400, "密码不能为空");
        } else {
            userById.setPassword(newPassword);
            userService.updateById(userById);
            return userById;
        }
    }

    @Override
    public void logout() {
        User user = TokenUtil.getUser();
        try {
            assert user != null;
            recordLogininfor(user.getUsername(), "0", "退出成功");
        } catch (Exception ignored) {
            recordLogininfor(user.getUsername(), "1", "退出失败");
        }
    }

    public User getUserInfo(UserDto userDto){
        String username = userDto.getUsername();
        String password = userDto.getPassword();
        if(StringUtils.isBlank(username) || StringUtils.isBlank(password)){
            throw new ServiceException(Constants.CODE_400,"参数错误");
        }
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", username);
        User one;
        try {
            one = getOne(queryWrapper);
        }catch (Exception e){
            //e.printStackTrace();
            //Log.get().error(e);
            throw new ServiceException(Constants.CODE_500,"系统异常");
        }
        return one;
    }

    /**
     * 记录登录信息
     *
     * @param username 用户名
     * @param status   状态
     * @param message  消息内容
     * @return
     */
    private void recordLogininfor(String username, String status, String message) {
        LogininforEvent logininforEvent = new LogininforEvent();
        logininforEvent.setUsername(username);
        logininforEvent.setStatus(status);
        logininforEvent.setMessage(message);
        logininforEvent.setRequest(ServletUtils.getRequest());
        SpringUtils.context().publishEvent(logininforEvent);
    }


}
