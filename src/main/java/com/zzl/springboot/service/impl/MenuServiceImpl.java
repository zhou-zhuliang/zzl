package com.zzl.springboot.service.impl;

import com.zzl.springboot.entity.Menu;
import com.zzl.springboot.mapper.MenuMapper;
import com.zzl.springboot.service.IMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Zzl
 * @since 2024-03-31
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {

    public List<Menu> buildMenuTree(List<Menu> menus) {
        // 创建一个Map，用于通过id快速查找菜单项
        Map<Integer, Menu> menuMap = new HashMap<>();
        for (Menu menu : menus) {
            menuMap.put(menu.getId(), menu);
        }

        // 找出所有的根节点（没有父节点的菜单项）
        List<Menu> tree = new ArrayList<>();
        for (Menu menu : menus) {
            if (menu.getParentId() == null) {
                tree.add(menu);
            }
        }

        // 为每个根节点添加子节点
        for (Menu menu : tree) {
            addChildren(menu, menuMap,1);
        }

        return tree;
    }

    private static void addChildren(Menu parent, Map<Integer, Menu> menuMap, int currentLevel ) {
        List<Menu> children = new ArrayList<>();
        for (Menu menu : menuMap.values()) {
            if (menu.getParentId() != null && menu.getParentId().equals(parent.getId())) {
                    children.add(menu);
            }
            // 根据nm_order字段对子节点进行排序
            //children.sort(Comparator.comparingInt(Menu::getNmOrder));
            children.sort(Comparator.comparing(Menu -> Menu.getNmOrder() != null ? Menu.getNmOrder() : 0, Comparator.naturalOrder()));



            if (currentLevel < 10) { // 检查当前层数是否小于10
                parent.setChildren(children); // 设置父节点的子节点列表
                for (Menu child : children) {
                    addChildren(child, menuMap, currentLevel + 1); // 递归添加子节点的子节点，层数加1
                }
            }
        }

    }

}
