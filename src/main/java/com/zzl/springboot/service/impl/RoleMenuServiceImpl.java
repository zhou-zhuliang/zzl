package com.zzl.springboot.service.impl;

import com.zzl.springboot.entity.RoleMenu;
import com.zzl.springboot.mapper.RoleMenuMapper;
import com.zzl.springboot.service.IRoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Zzl
 * @since 2024-04-02
 */
@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenu> implements IRoleMenuService {

}
