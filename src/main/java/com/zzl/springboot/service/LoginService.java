package com.zzl.springboot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zzl.springboot.controller.dto.PasswordDto;
import com.zzl.springboot.controller.dto.UserDto;
import com.zzl.springboot.entity.User;

/**
 * <p>
 *  登录服务类
 * </p>
 *
 * @author Zzl
 * @since 2024-03-21
 */
public interface LoginService extends IService<User> {


    UserDto login(UserDto userDto);

    User register(UserDto userDto);

    User editPw(PasswordDto passwordDto);

    void logout();
}
