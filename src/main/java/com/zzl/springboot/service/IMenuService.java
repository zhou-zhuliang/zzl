package com.zzl.springboot.service;

import com.zzl.springboot.entity.Menu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Zzl
 * @since 2024-03-31
 */
public interface IMenuService extends IService<Menu> {

    List<Menu> buildMenuTree(List<Menu> list);
}
