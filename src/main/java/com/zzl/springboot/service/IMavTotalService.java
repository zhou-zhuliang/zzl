package com.zzl.springboot.service;

import com.zzl.springboot.entity.MavTotal;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Zzl
 * @since 2024-04-19
 */
public interface IMavTotalService extends IService<MavTotal> {

}
