package com.zzl.springboot.service;

import com.zzl.springboot.entity.OperLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Zzl
 * @since 2024-04-22
 */
public interface IOperLogService extends IService<OperLog> {

}
