package com.zzl.springboot.service;

import com.zzl.springboot.entity.Logininfor;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Zzl
 * @since 2024-04-17
 */
public interface ILogininforService extends IService<Logininfor> {

}
