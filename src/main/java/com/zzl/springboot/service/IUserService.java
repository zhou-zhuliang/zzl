package com.zzl.springboot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zzl.springboot.entity.User;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Zzl
 * @since 2024-03-17
 */
public interface IUserService extends IService<User> {

}
