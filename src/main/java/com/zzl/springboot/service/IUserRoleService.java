package com.zzl.springboot.service;

import com.zzl.springboot.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Zzl
 * @since 2024-04-02
 */
public interface IUserRoleService extends IService<UserRole> {

}
