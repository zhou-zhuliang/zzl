package com.zzl.springboot.service;

import com.zzl.springboot.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Zzl
 * @since 2024-03-31
 */
public interface IRoleService extends IService<Role> {

}
