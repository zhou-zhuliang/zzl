package com.zzl.springboot.config;

import com.zzl.springboot.config.interceptors.JwtInterceptors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    @Autowired
    private JwtInterceptors jwtInterceptors;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(jwtInterceptors)
                // 放行接口
                .excludePathPatterns("/login", "/register","/logout")
                // swagger相关
                .excludePathPatterns("/swagger-resources/**", "/v2/api-docs/**","/swagger-ui/**")
                // 拦截器接口
                .excludePathPatterns("/error")

               // .excludePathPatterns("/**")

                // 拦截接口
                .addPathPatterns("/**");
    }
}
