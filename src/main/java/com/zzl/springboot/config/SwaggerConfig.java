package com.zzl.springboot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {


    @Bean
    public Docket adminApiConfig(){

        return new Docket(DocumentationType.SWAGGER_2)// DocumentationType.SWAGGER_2 固定的，代表swagger2
                .groupName("adminApi")// 如果配置多个文档的时候，那么需要配置groupName来分组标识
                .apiInfo(adminApiInfo())// 用于生成API信息
                .select()// select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .paths(PathSelectors.any())// 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .apis(RequestHandlerSelectors.basePackage("com.zzl.springboot.controller")) // 用于指定扫描哪个包下的接口
                .build();

    }

    private ApiInfo adminApiInfo(){

        return new ApiInfoBuilder()
                .title("后台管理系统API文档")// 可以用来自定义API的主标题
                .description("本文档描述了后台管理系统接口定义")// 可以用来描述整体的API
                .version("1.0")// 可以用来定义版本
                .contact(new Contact("Zzl", "localhost:9090", "258066liang@163.com"))
                .termsOfServiceUrl("http://localhost:9090/doc.html") // 用于定义服务的域名,不是访问主页路径
                .build();
    }

     /*@Bean
    public Docket webApiConfig(){

        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("webApi")
                .apiInfo(webApiInfo())
                .select()
                //过滤掉admin路径下的所有页面
                //.paths(Predicates.not(PathSelectors.regex("/admin/.*")))
                //过滤掉所有error或error.*页面
                //.paths(Predicates.not(PathSelectors.regex("/error.*")))
                .build();
    }*/

    /*private ApiInfo webApiInfo(){

        return new ApiInfoBuilder()
                .title("网站-课程中心API文档")
                .description("本文档描述了课程中心微服务接口定义")
                .version("1.0")
                .contact(new Contact("Zzl", "http://atguigu.com", "1416412681@qq.com"))
                .build();
    }*/


}