package com.zzl.springboot.config;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.zzl.springboot.entity.User;
import com.zzl.springboot.service.IUserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Component
public class TokenUtil {

    //声明对象
    public static TokenUtil tokenUtil;

    public static IUserService staticUserService;

    @Resource
    private IUserService userService;

    @Value("${jwt.secretKey}")
    private String secretKey;

    @Value("${jwt.expireTime}")
    private String expireTime;

    //初始化
    @PostConstruct
    public void init() {
        tokenUtil = this;
    }

    @PostConstruct
    public void setUserService() {
        staticUserService = userService;
    }


    /**
     * 加密生成token
     */
    public static String getToken(String userId) {
        return JWT
                .create()
                .withClaim("userId", userId)
                .withClaim("timeStamp", System.currentTimeMillis())
                .withExpiresAt(DateUtil.offsetHour(new Date(), Integer.parseInt(tokenUtil.expireTime))) // 设置超时时间
                .sign(Algorithm.HMAC256(tokenUtil.secretKey));
    }

    /**
     * 获取当前登录用户的信息
     */
    public static User getUser() {
        try {
            HttpServletRequest request = ((ServletRequestAttributes) (RequestContextHolder.getRequestAttributes())).getRequest();
            String token = request.getHeader("token");
            if (StrUtil.isNotBlank(token)){
                String userId = JWT.decode(token).getClaims().get("userId").asString();
                return staticUserService.getById(userId);
            }
        }catch (Exception e){
            return null;
        }
        return null;
    }



}
