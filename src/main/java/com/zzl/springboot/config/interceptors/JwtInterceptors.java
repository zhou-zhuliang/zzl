package com.zzl.springboot.config.interceptors;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.zzl.springboot.common.Constants;
import com.zzl.springboot.config.AuthAccess;
import com.zzl.springboot.config.TokenUtil;
import com.zzl.springboot.entity.User;
import com.zzl.springboot.exception.ServiceException;
import com.zzl.springboot.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class JwtInterceptors implements HandlerInterceptor {

    @Autowired
    private IUserService userService;

    @Value("${jwt.secretKey}")
    private String secretKey;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        //从http请求头中得到token
        String token = request.getHeader("token");

        //如果不是映射到方法直接通过
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }else {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            AuthAccess authAccess = handlerMethod.getMethodAnnotation(AuthAccess.class);
            if (authAccess != null){
                return true;
            }
        }

        // 没有token报错
        if (StringUtils.isBlank(token)) {
            throw new ServiceException(Constants.CODE_401, "token不存在");
        }
        // token解析失败报错
        String userId = "";
        try {
            userId = JWT.decode(token).getClaims().get("userId").asString();
        } catch (JWTVerificationException e) {
            throw new ServiceException(Constants.CODE_401, "token解析失败");
        }
        // 根据token中的userId查询数据库
        User user = userService.getById(userId);
        if (user == null) {
            throw new ServiceException(Constants.CODE_401, "用户不存在");
        }
        // 使用sign验证token
        JWTVerifier verifier = JWT.require(Algorithm.HMAC256(secretKey)).build();
        try {
            verifier.verify(token);//验证token
        } catch (Exception e) {
            throw new ServiceException(Constants.CODE_401, "token验证失败");
        }
        return true;
    }

}
