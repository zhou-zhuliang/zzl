package com.zzl.springboot.common;

public interface Constants {
    //成功
    int CODE_200 = 200;

    //参数错误
    int CODE_400 = 400;

    //权限不足
    int CODE_401 = 401;

    //失败
    int CODE_500 = 500;

    //其他业务异常
    int CODE_600 = 600;

    //系统警告信息
    int CODE_601 = 601;
}

