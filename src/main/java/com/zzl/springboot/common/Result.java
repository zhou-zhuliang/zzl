package com.zzl.springboot.common;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 响应信息主体
 *
 * @author Zzl
 */
@Data
@NoArgsConstructor
public class Result<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    private int code;

    private String msg;

    private T data;

    public static <T> Result<T> ok() {
        return restResult(null, Constants.CODE_200, "操作成功");
    }

    public static <T> Result<T> ok(T data) {
        return restResult(data, Constants.CODE_200, "操作成功");
    }

    public static <T> Result<T> ok(String msg) {
        return restResult(null, Constants.CODE_200, msg);
    }

    public static <T> Result<T> ok(String msg, T data) {
        return restResult(data, Constants.CODE_200, msg);
    }

    public static <T> Result<T> fail() {
        return restResult(null, Constants.CODE_500, "操作失败");
    }

    public static <T> Result<T> fail(String msg) {
        return restResult(null, Constants.CODE_500, msg);
    }

    public static <T> Result<T> fail(T data) {
        return restResult(data, Constants.CODE_500, "操作失败");
    }

    public static <T> Result<T> fail(String msg, T data) {
        return restResult(data, Constants.CODE_500, msg);
    }

    public static <T> Result<T> fail(int code, String msg) {
        return restResult(null, code, msg);
    }

    /**
     * 返回警告消息
     *
     * @param msg 返回内容
     * @return 警告消息
     */
    public static <T> Result<T> warn(String msg) {
        return restResult(null, Constants.CODE_601, msg);
    }

    /**
     * 返回警告消息
     *
     * @param msg 返回内容
     * @param data 数据对象
     * @return 警告消息
     */
    public static <T> Result<T> warn(String msg, T data) {
        return restResult(data, Constants.CODE_601, msg);
    }

    private static <T> Result<T> restResult(T data, int code, String msg) {
        Result<T> r = new Result<>();
        r.setCode(code);
        r.setData(data);
        r.setMsg(msg);
        return r;
    }

    public static <T> Boolean isError(Result<T> ret) {
        return !isSuccess(ret);
    }

    public static <T> Boolean isSuccess(Result<T> ret) {
        return Constants.CODE_200 == ret.getCode();
    }
}
