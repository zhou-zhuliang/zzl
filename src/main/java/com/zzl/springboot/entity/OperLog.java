package com.zzl.springboot.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author Zzl
 * @since 2024-04-22
 */
@Data
@TableName("sys_oper_log")
@ApiModel(value = "OperLog对象", description = "")
public class OperLog implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("日志主键")
      private String operId;

      @ApiModelProperty("模块标题")
      private String title;

      @ApiModelProperty("业务类型（0其它 1新增 2修改 3删除）")
      private BigDecimal businessType;

      @ApiModelProperty("方法名称")
      private String method;

      @ApiModelProperty("请求方式")
      private String requestMethod;

      @ApiModelProperty("操作类别（0其它 1后台用户 2手机端用户）")
      private BigDecimal operatorType;

      @ApiModelProperty("操作人员")
      private String operName;

      @ApiModelProperty("请求URL")
      private String operUrl;

      @ApiModelProperty("主机地址")
      private String operIp;

      @ApiModelProperty("操作地点")
      private String operLocation;

      @ApiModelProperty("请求参数")
      private String operParam;

      @ApiModelProperty("返回参数")
      private String jsonResult;

      @ApiModelProperty("操作状态（0正常 1异常）")
      private BigDecimal status;

      @ApiModelProperty("错误消息")
      private String errorMsg;

      @ApiModelProperty("操作时间")
      private Date operTime;


}
