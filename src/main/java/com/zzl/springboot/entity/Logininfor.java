package com.zzl.springboot.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author Zzl
 * @since 2024-04-17
 */
@Data
@TableName("sys_logininfor")
@ApiModel(value = "Logininfor对象", description = "")
public class Logininfor implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("访问ID")
    private String infoId;

    @ApiModelProperty("用户ID")
    private String userId;

    @ApiModelProperty("用户账号")
    private String userName;

    @ApiModelProperty("登录IP地址")
    private String ipaddr;

    @ApiModelProperty("登录地点")
    private String loginLocation;

    @ApiModelProperty("浏览器类型")
    private String browser;

    @ApiModelProperty("操作系统")
    private String os;

    @ApiModelProperty("登录状态（0成功 1失败）")
    private String status;

    @ApiModelProperty("提示消息")
    private String msg;

    @ApiModelProperty("访问时间")
    private Date loginTime;


}
