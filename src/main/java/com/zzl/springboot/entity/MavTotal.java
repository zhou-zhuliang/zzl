package com.zzl.springboot.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author Zzl
 * @since 2024-04-19
 */
@Data
  @TableName("sys_mav_total")
@ApiModel(value = "MavTotal对象", description = "")
public class MavTotal implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("用户id")
      @TableId(value = "userid")
      private Integer userid;

      @ApiModelProperty("总数")
      private String total;


}
