package com.zzl.springboot.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author Zzl
 * @since 2024-04-02
 */
@Data
@TableName("sys_user_role")
@ApiModel(value = "UserRole对象", description = "")
public class UserRole implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("用户id")
        private Integer userId;

      @ApiModelProperty("角色id")
        private Integer roleId;


}
