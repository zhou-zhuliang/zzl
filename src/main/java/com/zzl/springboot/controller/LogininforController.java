package com.zzl.springboot.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zzl.springboot.common.Result;
import com.zzl.springboot.entity.Logininfor;
import com.zzl.springboot.service.ILogininforService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Zzl
 * @since 2024-04-17
 */
@RestController
@RequestMapping("/logininfor")
public class LogininforController {

    @Autowired
    private ILogininforService logininforService;

    //新增或修改
    @PostMapping
    @ApiOperation(value="新增和修改")
    public Result<Object> saveOrUpdate(@RequestBody Logininfor logininfor) {
        return Result.ok(logininforService.saveOrUpdate(logininfor));
    }

    //查询所有数据
    @GetMapping
    @ApiOperation(value="查询所有数据")
    public Result<List<Logininfor>> findAllLogininfor() {
        return Result.ok(logininforService.list());
    }

    //根据id查询
    @GetMapping("/{id}")
    @ApiOperation(value="根据id查询")
    public Result<Logininfor> findOne(@PathVariable Integer id) {
        return Result.ok(logininforService.getById(id));
    }

    //分页查询
    @GetMapping("/page")
    @ApiOperation(value="分页查询")
    public Result<IPage<Logininfor>> selectPage(@RequestParam Integer pageNum,
                                                @RequestParam Integer pageSize,
                                                @RequestParam String userName,
                                                @RequestParam String status) {
        IPage<Logininfor> logininforPage = new Page<>(pageNum, pageSize);
        QueryWrapper<Logininfor> queryWrapper = new QueryWrapper<>();
        if(StringUtils.isNotBlank(userName)){
            queryWrapper.like("user_name", userName);
        }
        if(StringUtils.isNotBlank(status)){
            queryWrapper.eq("status", status);
        }
        queryWrapper.orderByDesc("login_time");
        IPage<Logininfor> page = logininforService.page(logininforPage, queryWrapper);
        return Result.ok(page);



    }

    //根据id删除
    @DeleteMapping("/{id}")
    @ApiOperation(value="根据id删除")
    public Result<Object> deleteByIdLogininfor(@PathVariable("id") Integer id) {
        return Result.ok(logininforService.removeById(id));
    }

    //批量删除
    @PostMapping("/batchDelete")
    @ApiOperation(value="批量删除用户")
    public Result<Object> deleteBatchLogininfor(@RequestBody List<Integer> ids) {
        return Result.ok(logininforService.removeByIds(ids));
    }
}

