package com.zzl.springboot.controller.dto;

import lombok.Data;

import java.util.List;

@Data
public class UserRoleDto {
    private Integer userId;
    private List<Integer> roleIds;
}
