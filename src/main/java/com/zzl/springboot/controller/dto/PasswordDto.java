package com.zzl.springboot.controller.dto;

import lombok.Data;

/**
 * 接受前端登录请求的参数
 */
@Data
public class PasswordDto {

    //密码
    private String oldPassword;

    //新密码
    private String newPassword;



}
