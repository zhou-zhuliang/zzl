package com.zzl.springboot.controller.dto;

import lombok.Data;

/**
 * 接受前端登录请求的参数
 */
@Data
public class UserDto {

    //id
    private Integer id;

    //用户名
    private String username;

    //密码
    private String password;

    //昵称
    private String nickname;

    //头像
    private String avatarUrl;

    //token
    private String token;


}
