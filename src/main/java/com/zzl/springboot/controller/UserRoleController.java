package com.zzl.springboot.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zzl.springboot.common.Result;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

import com.zzl.springboot.service.IUserRoleService;
import com.zzl.springboot.entity.UserRole;

import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Zzl
 * @since 2024-04-02
 */
@RestController
@RequestMapping("/user-role")
public class UserRoleController {

    @Autowired
    private IUserRoleService userRoleService;

    //新增或修改
    @PostMapping
    @ApiOperation(value="新增和修改")
    public Result<Object> saveOrUpdate(@RequestBody UserRole userRole) {
        return Result.ok(userRoleService.saveOrUpdate(userRole));
    }

    //查询所有数据
    @GetMapping
    @ApiOperation(value="查询所有数据")
    public Result<List<UserRole>> findAllUserRole() {
        return Result.ok(userRoleService.list());
    }

    //根据id查询
    @GetMapping("/{id}")
    @ApiOperation(value="根据id查询")
    public Result<UserRole> findOne(@PathVariable Integer id) {
        return Result.ok(userRoleService.getById(id));
    }

    //分页查询
    @GetMapping("/page")
    @ApiOperation(value="分页查询")
    public Result<IPage<UserRole>> selectPage(@RequestParam Integer pageNum,
                                       @RequestParam Integer pageSize) {
        IPage<UserRole> userRolePage = new Page<>(pageNum, pageSize);
        QueryWrapper<UserRole> queryWrapper = new QueryWrapper<>();
        return Result.ok(userRoleService.page(userRolePage, queryWrapper));
    }

    //根据id删除
    @DeleteMapping("/{id}")
    @ApiOperation(value="根据id删除")
    public Result<Object> deleteByIdUserRole(@PathVariable("id") Integer id) {
        return Result.ok(userRoleService.removeById(id));
    }

    //批量删除
    @PostMapping("/batchDelete")
    @ApiOperation(value="批量删除用户")
    public Result<Object> deleteBatchUserRole(@RequestBody List<Integer> ids) {
        return Result.ok(userRoleService.removeByIds(ids));
    }
}

