package com.zzl.springboot.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zzl.springboot.common.Result;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

import com.zzl.springboot.service.IRoleMenuService;
import com.zzl.springboot.entity.RoleMenu;

import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Zzl
 * @since 2024-04-02
 */
@RestController
@RequestMapping("/role-menu")
public class RoleMenuController {

    @Autowired
    private IRoleMenuService roleMenuService;

    //新增或修改
    @PostMapping
    @ApiOperation(value="新增和修改")
    public Result<Object> saveOrUpdate(@RequestBody RoleMenu roleMenu) {
        return Result.ok(roleMenuService.saveOrUpdate(roleMenu));
    }

    //查询所有数据
    @GetMapping
    @ApiOperation(value="查询所有数据")
    public Result<List<RoleMenu>> findAllRoleMenu() {
        return Result.ok(roleMenuService.list());
    }

    //根据id查询
    @GetMapping("/{id}")
    @ApiOperation(value="根据id查询")
    public Result<RoleMenu> findOne(@PathVariable Integer id) {
        return Result.ok(roleMenuService.getById(id));
    }

    //分页查询
    @GetMapping("/page")
    @ApiOperation(value="分页查询")
    public Result<IPage<RoleMenu>> selectPage(@RequestParam Integer pageNum,
                                       @RequestParam Integer pageSize) {
        IPage<RoleMenu> roleMenuPage = new Page<>(pageNum, pageSize);
        QueryWrapper<RoleMenu> queryWrapper = new QueryWrapper<>();
        return Result.ok(roleMenuService.page(roleMenuPage, queryWrapper));
    }

    //根据id删除
    @DeleteMapping("/{id}")
    @ApiOperation(value="根据id删除")
    public Result<Object> deleteByIdRoleMenu(@PathVariable("id") Integer id) {
        return Result.ok(roleMenuService.removeById(id));
    }

    //批量删除
    @PostMapping("/batchDelete")
    @ApiOperation(value="批量删除用户")
    public Result<Object> deleteBatchRoleMenu(@RequestBody List<Integer> ids) {
        return Result.ok(roleMenuService.removeByIds(ids));
    }
}

