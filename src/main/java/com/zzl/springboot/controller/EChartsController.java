package com.zzl.springboot.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.Quarter;
import com.zzl.springboot.common.Result;
import com.zzl.springboot.config.Log;
import com.zzl.springboot.entity.User;
import com.zzl.springboot.enums.BusinessType;
import com.zzl.springboot.service.IUserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/echarts")
public class EChartsController {

    @Autowired
    private IUserService userService;

    @PostMapping("/example")
    public Result<Map<String, Object>> get() {
        Map<String, Object> map = new HashMap<>();
        map.put("x", CollUtil.newArrayList("Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"));
        map.put("y", CollUtil.newArrayList(150, 230, 224, 218, 135, 147, 260));
        return Result.ok(map);
    }

    /**
     * 查询每个季度创建用户的人数
     * @return
     */
    @Log(title = "图表统计", businessType = BusinessType.OTHER)
    @PostMapping("/members")
    @ApiOperation(value="查询每个季度创建用户的人数")
    public Result<Object> members() {
        List<User> userList = userService.list();
        int q1 = 0;
        int q2 = 0;
        int q3 = 0;
        int q4 = 0;
        for (User user:userList) {
            Date createTime = user.getCreateTime();
            Quarter quarter = DateUtil.quarterEnum(createTime);// 获得指定日期所属季度
            switch (quarter){
                case Q1: q1 += 1; break;
                case Q2: q2 += 1; break;
                case Q3: q3 += 1; break;
                case Q4: q4 += 1; break;
                default: break;
            }
        }
        return Result.ok(CollUtil.newArrayList(q1,q2,q3,q4));

    }
}
