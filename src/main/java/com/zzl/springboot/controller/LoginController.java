package com.zzl.springboot.controller;

import com.zzl.springboot.common.Result;
import com.zzl.springboot.config.TokenUtil;
import com.zzl.springboot.controller.dto.PasswordDto;
import com.zzl.springboot.controller.dto.UserDto;
import com.zzl.springboot.entity.User;
import com.zzl.springboot.service.LoginService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;

    /**
     * 登录
     * @param userDto
     * @return Result
     */
    @PostMapping("/login")
    @ApiOperation(value="登录")
    public Result<Object> login(@RequestBody UserDto userDto){
        UserDto login = loginService.login(userDto);
        return Result.ok(login);
    }

    /**
     * 注册
     * @param userDto
     * @return Result
     */
    @PostMapping("/register")
    @ApiOperation(value="注册")
    public Result<Object> register(@RequestBody UserDto userDto){
        User login = loginService.register(userDto);
        return Result.ok(login);
    }

    /**
     * 退出登录
     */
    @PostMapping("/logout")
    @ApiOperation(value="退出登录")
    public Result<Object> logout() {
        loginService.logout();
        return Result.ok("退出成功");
    }

    /**
     *
     * @param passwordDto
     * @return Result
     */
    @PostMapping("/editPw")
    @ApiOperation(value="修改密码")
    public Result<Object> editPw(@RequestBody PasswordDto passwordDto){
        loginService.editPw(passwordDto);
        return Result.ok("修改成功");
    }


}
