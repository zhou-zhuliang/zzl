package com.zzl.springboot.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zzl.springboot.common.Result;
import com.zzl.springboot.config.Log;
import com.zzl.springboot.entity.Files;
import com.zzl.springboot.enums.BusinessType;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/my")
public class MyController {

    @Log(title = "主页", businessType = BusinessType.OTHER)
    @PostMapping("/home")
    public Result<String> home() {
        return Result.ok("主页");
    }

    @Log(title = "高德地图", businessType = BusinessType.OTHER)
    @PostMapping("/map")
    public Result<String> map() {
        return Result.ok("高德地图");
    }

    @Log(title = "我是秦始皇", businessType = BusinessType.OTHER)
    @PostMapping("/money")
    public Result<String> money() {
        return Result.ok("我是秦始皇");
    }

}
