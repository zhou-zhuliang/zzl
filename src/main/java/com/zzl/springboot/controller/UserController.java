package com.zzl.springboot.controller;

import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zzl.springboot.common.Result;
import com.zzl.springboot.config.AuthAccess;
import com.zzl.springboot.config.Log;
import com.zzl.springboot.entity.User;
import com.zzl.springboot.enums.BusinessType;
import com.zzl.springboot.service.IUserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Zzl
 * @since 2024-03-17
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService userService;

    //新增或修改
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @PostMapping
    @ApiOperation(value="新增和修改")
    public Result<User> saveOrUpdate(@RequestBody User user) {
        if (user.getId() == null){
            user.setPassword("123456");
        }
        userService.saveOrUpdate(user);
        return Result.ok(user);
    }

    //查询所有数据
    @GetMapping
    @ApiOperation(value="查询所有数据")
    public List<User> findAllUser() {
        return userService.list();
    }

    //根据id查询
    @GetMapping("/{id}")
    @ApiOperation(value="根据id查询")
    public User findOne(@PathVariable Integer id) {
        return userService.getById(id);
    }

    //分页查询
    @GetMapping("/page")
    @ApiOperation(value="分页查询")
    public IPage<User> selectPage(@RequestParam Integer pageNum,
                                  @RequestParam Integer pageSize,
                                  @RequestParam(required = false) String name,
                                  @RequestParam(required = false) String email,
                                  @RequestParam(required = false) String address) {
        IPage<User> userPage = new Page<>(pageNum, pageSize);
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        if(StringUtils.isNotBlank(name)){
            queryWrapper.like("name", name);
        }
        if(StringUtils.isNotBlank(email)){
            queryWrapper.like("email", email);
        }
        if(StringUtils.isNotBlank(address)){
            queryWrapper.like("address", address);
        }
        queryWrapper.orderByDesc("id");

        return userService.page(userPage, queryWrapper);
    }

    //根据id删除
    @Log(title = "用户管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{id}")
    @ApiOperation(value="根据id删除")
    public boolean deleteByIdUser(@PathVariable("id") Integer id) {
        return userService.removeById(id);
    }

    //批量删除
    @Log(title = "用户管理", businessType = BusinessType.DELETE)
    @PostMapping("/batchDelete")
    @ApiOperation(value="批量删除用户")
    public boolean deleteBatchUser(@RequestBody List<Integer> ids) {
        return userService.removeByIds(ids);
    }

    //Excel导出
    @Log(title = "用户管理", businessType = BusinessType.EXPORT)
    @GetMapping("/exportExcel")
    @ApiOperation(value="Excel导出")
    public void export(HttpServletRequest req, HttpServletResponse res) throws IOException {
        //导出的数据
        String name = req.getParameter("name");
        String email = req.getParameter("email");
        String address = req.getParameter("address");
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        if(StringUtils.isNotBlank(name)){
            queryWrapper.like("name", name);
        }
        if(StringUtils.isNotBlank(email)){
            queryWrapper.like("email", email);
        }
        if(StringUtils.isNotBlank(address)){
            queryWrapper.like("address", address);
        }
        List<User> list = userService.list(queryWrapper);
        // 通过工具类创建writer 写出到磁盘路径
        //ExcelWriter writer = ExcelUtil.getWriter("d:/writeBeanTest.xlsx");
        // 通过工具类创建writer
        ExcelWriter writer = ExcelUtil.getWriter();

        // 自定义标题别名
       // writer.addHeaderAlias("id", "id");
      /*  writer.addHeaderAlias("username", "用户名");
        writer.addHeaderAlias("password", "密码");
        writer.addHeaderAlias("nickname", "昵称");
        writer.addHeaderAlias("email", "邮箱");
        writer.addHeaderAlias("phone", "电话");
        writer.addHeaderAlias("address", "地址");
        writer.addHeaderAlias("avatarUrl", "头像");
        writer.addHeaderAlias("createTime", "创建时间");*/

        // 默认的，未添加alias的属性也会写出，如果想只写出加了别名的字段，可以调用此方法排除之 true只写出有添加了alias属性的，@Alias不生效
        writer.setOnlyAlias(true);

        // 合并单元格后的标题行，使用默认标题样式
        //writer.merge(7, "用户信息");

        // 一次性写出内容，使用默认样式，强制输出标题
        writer.write(list, true);

        // 设置响应信息
        res.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8");
        String filename = URLEncoder.encode("用户信息", "UTF-8");
        res.setHeader("Content-Disposition", "attachment;filename=" + filename + ".xls");

        // 将数据流刷到out输出流中
        ServletOutputStream out = res.getOutputStream();
        writer.flush(out, true);

        //关闭流
        out.close();
        writer.close();
    }

    //Excel导入
    @Log(title = "用户管理", businessType = BusinessType.IMPORT)
    @PostMapping("/importExcel")
    @ApiOperation(value="Excel导入")
    public boolean importExcel(MultipartFile file) throws IOException {
        InputStream input = file.getInputStream();
        ExcelReader reader = ExcelUtil.getReader(input);

        //标题行和数据库一样 是英文的
        //List<User> userList = reader.readAll(User.class);

        //循环一列一列set
       /* List<List<Object>> excelList = reader.read(1);
        List<User> userList = new ArrayList<>();
        for(List<Object> row : excelList){
            User user = new User();
            user.setUsername(row.get(0).toString());
            user.setPassword(row.get(1).toString());
            user.setNickname(row.get(2).toString());
            user.setEmail(row.get(3).toString());
            user.setPhone(row.get(4).toString());
            user.setAddress(row.get(5).toString());
            user.setAvatarUrl(row.get(6).toString());
            userList.add(user);
        }*/

        //把Excel标题行中文转为英文 和数据库字段一样
        reader.addHeaderAlias("用户名","username");
        reader.addHeaderAlias("密码","password");
        reader.addHeaderAlias("姓名","name");
        reader.addHeaderAlias("昵称","nickname");
        reader.addHeaderAlias("邮箱","email");
        reader.addHeaderAlias("电话","phone");
        reader.addHeaderAlias("地址","address");
        reader.addHeaderAlias("头像","avatarUrl");
        List<User> userList = reader.read(0, 1, User.class);

        input.close();
        return userService.saveBatch(userList);

    }


}

