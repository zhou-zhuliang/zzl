package com.zzl.springboot.controller;

import cn.hutool.core.io.FileUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zzl.springboot.common.Result;
import com.zzl.springboot.config.AuthAccess;
import com.zzl.springboot.config.Log;
import com.zzl.springboot.entity.Files;
import com.zzl.springboot.enums.BusinessType;
import com.zzl.springboot.service.IFilesService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Zzl
 * @since 2024-03-26
 */
@RestController
@RequestMapping("/files")
public class FilesController {

    @Autowired
    private IFilesService filesService;

    @Value("${files.upload.path}")
    private String fileUploadPath;

    @Log(title = "照片墙", businessType = BusinessType.OTHER)
    @PostMapping("/list")
    @ApiOperation(value="获取文件列表")
    public Result<List<Files>> list(@RequestParam(required = false) Integer id ) {
        QueryWrapper<Files> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_delete", false);
        if(id != null){
            queryWrapper.eq("id", id);
        }
        return Result.ok(filesService.list(queryWrapper));
    }

    @Log(title = "福利视频", businessType = BusinessType.OTHER)
    @PostMapping("/list2")
    @ApiOperation(value="获取文件列表")
    public Result<List<Files>> list2(@RequestParam(required = false) Integer id ) {
        QueryWrapper<Files> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_delete", false);
        if(id != null){
            queryWrapper.eq("id", id);
        }
        return Result.ok(filesService.list(queryWrapper));
    }

    /**
     * 分页查询
     * @param pageNum
     * @param pageSize
     * @param name
     * @return
     */
    @PostMapping("/page")
    @ApiOperation(value="分页查询")
    public Result<IPage<Files>> selectPage(@RequestParam Integer pageNum,
                                           @RequestParam Integer pageSize,
                                           @RequestParam(required = false) String name) {
        IPage<Files> filesPage = new Page<>(pageNum, pageSize);
        QueryWrapper<Files> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(name)){
            queryWrapper.like("name",name);
        }
        queryWrapper.orderByDesc("id");
        queryWrapper.eq("is_delete", false);
        queryWrapper.eq("mark", true);
        return Result.ok(filesService.page(filesPage, queryWrapper));
    }

    /**
     * 更新启用状态
     * @param files
     * @return
     */
    @Log(title = "文件管理", businessType = BusinessType.UPDATE)
    @PostMapping("/updateEnable")
    @ApiOperation(value="更新启用状态")
    public Result<String> changeEnable(@RequestBody Files files){
        boolean b = filesService.saveOrUpdate(files);
        if (b){
            return Result.ok("更新成功");
        }else {
            return Result.fail("更新失败");
        }
    }

    /**
     * 批量逻辑删除
     * @param ids
     * @return
     */
    @Log(title = "文件管理", businessType = BusinessType.DELETE)
    @PostMapping("/batchDelete")
    @ApiOperation(value="根据id逻辑删除")
    public Result<String> deleteByIds(@RequestBody List<Integer> ids) {
       /* QueryWrapper<Files> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("id", ids);
        List<Files> list = filesService.list(queryWrapper);
        for (Files file:list) {
            file.setIsDelete(true);
        }
        boolean b = filesService.updateBatchById(list);
        if (b){
            return Result.ok("删除成功");
        }else {
            return Result.fail("删除失败");
        }*/
        boolean update = filesService.update(new UpdateWrapper<Files>()
                .lambda()
                .set(Files::getIsDelete, true)
                .in(Files::getId, ids));
        if (update){
            return Result.ok("删除成功");
        }else {
            return Result.fail("删除失败");
        }
    }

    /**
     * 文件上传
     * @param file
     * @return
     */
    @Log(title = "文件管理", businessType = BusinessType.IMPORT)
    @PostMapping("/uploadFile")
    @ApiOperation(value="文件上传")
    public Result<String> uploadFile(@RequestParam MultipartFile file) {
        String s = filesService.uploadFile(file);
        return Result.ok(s);
    }

    /**
     * 文件下载
     * @param fileUUID
     * @param response
     * @throws IOException
     */
    @AuthAccess
    @Log(title = "文件管理", businessType = BusinessType.EXPORT)
    @GetMapping("/{fileUUID}")
    @ApiOperation(value="文件下载")
    public void download(@PathVariable String fileUUID, HttpServletResponse response) throws IOException {
        // 根据文件的唯一标识获取文件
        File downFile = new File(fileUploadPath + fileUUID);
        // 设置输出流的格式
        ServletOutputStream outputStream = response.getOutputStream();
        response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileUUID, "UTF-8"));
        response.setContentType("application/octet-stream");
        //读取文件的字节流
        outputStream.write(FileUtil.readBytes(downFile));
        //关闭资源
        outputStream.flush();
        outputStream.close();
    }

}

