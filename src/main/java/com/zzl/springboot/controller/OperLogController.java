package com.zzl.springboot.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zzl.springboot.common.Result;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

import com.zzl.springboot.service.IOperLogService;
import com.zzl.springboot.entity.OperLog;

import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Zzl
 * @since 2024-04-22
 */
@RestController
@RequestMapping("/oper-log")
public class OperLogController {

    @Autowired
    private IOperLogService operLogService;

    //新增或修改
    @PostMapping
    @ApiOperation(value="新增和修改")
    public Result<Object> saveOrUpdate(@RequestBody OperLog operLog) {
        return Result.ok(operLogService.saveOrUpdate(operLog));
    }

    //查询所有数据
    @GetMapping
    @ApiOperation(value="查询所有数据")
    public Result<List<OperLog>> findAllOperLog() {
        return Result.ok(operLogService.list());
    }

    //根据id查询
    @GetMapping("/{id}")
    @ApiOperation(value="根据id查询")
    public Result<OperLog> findOne(@PathVariable Integer id) {
        return Result.ok(operLogService.getById(id));
    }

    //分页查询
    @GetMapping("/page")
    @ApiOperation(value="分页查询")
    public Result<IPage<OperLog>> selectPage(@RequestParam Integer pageNum,
                                             @RequestParam Integer pageSize,
                                             @RequestParam String title,
                                             @RequestParam String operName,
                                             @RequestParam String status
                                             ) {
        IPage<OperLog> operLogPage = new Page<>(pageNum, pageSize);
        QueryWrapper<OperLog> queryWrapper = new QueryWrapper<>();
        if(StringUtils.isNotBlank(title)){
            queryWrapper.like("title", title);
        }
        if(StringUtils.isNotBlank(operName)){
            queryWrapper.like("oper_name", operName);
        }
        if(StringUtils.isNotBlank(status)){
            queryWrapper.eq("status", status);
        }
        queryWrapper.orderByDesc("oper_time");
        return Result.ok(operLogService.page(operLogPage, queryWrapper));
    }

    //根据id删除
    @DeleteMapping("/{id}")
    @ApiOperation(value="根据id删除")
    public Result<Object> deleteByIdOperLog(@PathVariable("id") Integer id) {
        return Result.ok(operLogService.removeById(id));
    }

    //批量删除
    @PostMapping("/batchDelete")
    @ApiOperation(value="批量删除用户")
    public Result<Object> deleteBatchOperLog(@RequestBody List<Integer> ids) {
        return Result.ok(operLogService.removeByIds(ids));
    }
}

