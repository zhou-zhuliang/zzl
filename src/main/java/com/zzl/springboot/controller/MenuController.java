package com.zzl.springboot.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zzl.springboot.common.Result;
import com.zzl.springboot.config.Log;
import com.zzl.springboot.config.TokenUtil;
import com.zzl.springboot.controller.dto.RoleMenuDto;
import com.zzl.springboot.entity.Dict;
import com.zzl.springboot.entity.Menu;
import com.zzl.springboot.entity.RoleMenu;
import com.zzl.springboot.entity.User;
import com.zzl.springboot.enums.BusinessType;
import com.zzl.springboot.mapper.DictMapper;
import com.zzl.springboot.mapper.MenuMapper;
import com.zzl.springboot.mapper.RoleMenuMapper;
import com.zzl.springboot.service.IMenuService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Zzl
 * @since 2024-03-31
 */
@RestController
@RequestMapping("/menu")
public class MenuController {

    @Autowired
    private IMenuService menuService;

    @Resource
    private MenuMapper menuMapper;

    @Resource
    private RoleMenuMapper roleMenuMapper;

    @Resource
    private DictMapper dictMapper;

    //根据id查询
    @GetMapping("/icons")
    @ApiOperation(value="查询图标")
    public Result<List<Dict>> getIcons() {
        QueryWrapper<Dict> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("type", "icon");
        return Result.ok(dictMapper.selectList(queryWrapper));
    }


    @GetMapping("/selectMenusByRoleId")
    @ApiOperation(value="根据角色id查询菜单id")
    public Result<Object> selectMenusByRoleId(@RequestParam Integer roleId) {
        return Result.ok(roleMenuMapper.selectMenusByRoleId(roleId));
    }

    @Log(title = "角色管理", businessType = BusinessType.UPDATE)
    @PostMapping("/setRoleMenu")
    @Transactional
    @ApiOperation(value="添加角色关联菜单")
    public Result<Object> setRoleMenu(@RequestBody RoleMenuDto roleMenuDto) {
        //先删除当前角色下的所有菜单
        roleMenuMapper.deleteByRoleId(roleMenuDto.getRoleId());
        //保存
        for (Integer menuId:roleMenuDto.getMenuIds()) {
            RoleMenu roleMenu = new RoleMenu();
            roleMenu.setRoleId(roleMenuDto.getRoleId());
            roleMenu.setMenuId(menuId);
            roleMenuMapper.insert(roleMenu);
        }
        return Result.ok();
    }

    /**
     * 返回所有菜单树状结构
     * @return
     */
    @PostMapping("/getMenuTree")
    @ApiOperation(value="返回所有菜单树状结构")
    public Result<Object> treeMenu(@RequestParam(required = false) String isAll){
        User user = TokenUtil.getUser();
        List<Menu> list = null;
        if (user != null) {
            if (StringUtils.isNotBlank(isAll) || user.getUsername().equals("admin") ){
                list = menuService.list();
            }else {
                list = menuMapper.selectMenusByUserId(user.getId());
            }
        }
        List<Menu> menus = menuService.buildMenuTree(list);
        return Result.ok(menus);
    }

    //新增或修改
    @Log(title = "菜单管理", businessType = BusinessType.UPDATE)
    @PostMapping
    @ApiOperation(value="新增和修改")
    public Result<Object> saveOrUpdate(@RequestBody Menu menu) {
        return Result.ok(menuService.saveOrUpdate(menu));
    }

    //查询所有数据
    @GetMapping
    @ApiOperation(value="查询所有数据")
    public Result<List<Menu>> findAllMenu() {
        return Result.ok(menuService.list());
    }

    //根据id查询
    @GetMapping("/{id}")
    @ApiOperation(value="根据id查询")
    public Result<Menu> findOne(@PathVariable Integer id) {
        return Result.ok(menuService.getById(id));
    }

    //分页查询
    @GetMapping("/page")
    @ApiOperation(value="分页查询")
    public Result<IPage<Menu>> selectPage(@RequestParam Integer pageNum,
                                          @RequestParam Integer pageSize,
                                          @RequestParam(required = false) String menuName) {
        IPage<Menu> menuPage = new Page<>(pageNum, pageSize);
        QueryWrapper<Menu> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(menuName)){
            queryWrapper.like("menu_name", menuName);
        }
        queryWrapper.orderByDesc("nm_order");
        return Result.ok(menuService.page(menuPage, queryWrapper));
    }

    //根据id删除
    @Log(title = "菜单管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{id}")
    @ApiOperation(value="根据id删除")
    public Result<Object> deleteByIdMenu(@PathVariable("id") Integer id) {
        return Result.ok(menuService.removeById(id));
    }

    //批量删除
    @Log(title = "菜单管理", businessType = BusinessType.DELETE)
    @PostMapping("/batchDelete")
    @ApiOperation(value="批量删除用户")
    public Result<Object> deleteBatchMenu(@RequestBody List<Integer> ids) {
        return Result.ok(menuService.removeByIds(ids));
    }
}

