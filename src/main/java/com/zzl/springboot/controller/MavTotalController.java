package com.zzl.springboot.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zzl.springboot.common.Result;
import com.zzl.springboot.config.AuthAccess;
import com.zzl.springboot.config.Log;
import com.zzl.springboot.enums.BusinessType;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

import com.zzl.springboot.service.IMavTotalService;
import com.zzl.springboot.entity.MavTotal;

import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Zzl
 * @since 2024-04-19
 */
@RestController
@RequestMapping("/mav-total")
public class MavTotalController {

    @Autowired
    private IMavTotalService mavTotalService;

    //新增或修改
    @AuthAccess
    @PostMapping
    @ApiOperation(value="新增和修改")
    public Result<Object> saveOrUpdate(@RequestBody MavTotal mavTotal) {
        return Result.ok(mavTotalService.saveOrUpdate(mavTotal));
    }

    //查询所有数据
    @GetMapping
    @ApiOperation(value="查询所有数据")
    public Result<List<MavTotal>> findAllMavTotal() {
        return Result.ok(mavTotalService.list());
    }

    //根据id查询
    @AuthAccess
    @Log(title = "木鱼", businessType = BusinessType.OTHER)
    @GetMapping("/{userid}")
    @ApiOperation(value="根据id查询")
    public Result<MavTotal> findOne(@PathVariable Integer userid) {
        return Result.ok(mavTotalService.getById(userid));
    }

    //分页查询
    @GetMapping("/page")
    @ApiOperation(value="分页查询")
    public Result<IPage<MavTotal>> selectPage(@RequestParam Integer pageNum,
                                       @RequestParam Integer pageSize) {
        IPage<MavTotal> mavTotalPage = new Page<>(pageNum, pageSize);
        QueryWrapper<MavTotal> queryWrapper = new QueryWrapper<>();
        return Result.ok(mavTotalService.page(mavTotalPage, queryWrapper));
    }

    //根据id删除
    @DeleteMapping("/{id}")
    @ApiOperation(value="根据id删除")
    public Result<Object> deleteByIdMavTotal(@PathVariable("id") Integer id) {
        return Result.ok(mavTotalService.removeById(id));
    }

    //批量删除
    @PostMapping("/batchDelete")
    @ApiOperation(value="批量删除用户")
    public Result<Object> deleteBatchMavTotal(@RequestBody List<Integer> ids) {
        return Result.ok(mavTotalService.removeByIds(ids));
    }
}

