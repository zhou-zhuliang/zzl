package com.zzl.springboot.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zzl.springboot.common.Result;
import com.zzl.springboot.config.Log;
import com.zzl.springboot.controller.dto.UserRoleDto;
import com.zzl.springboot.entity.Role;
import com.zzl.springboot.entity.UserRole;
import com.zzl.springboot.enums.BusinessType;
import com.zzl.springboot.mapper.UserRoleMapper;
import com.zzl.springboot.service.IRoleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Zzl
 * @since 2024-03-31
 */
@RestController
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private IRoleService roleService;

    @Resource
    private UserRoleMapper userRoleMapper;

    @PostMapping("/selectRolesByUserId")
    @ApiOperation(value="根据用户id查询角色列表")
    public Result<Object> selectRolesByUserId(@RequestBody Integer userId) {
        return Result.ok(userRoleMapper.selectRolesByUserId(userId));
    }

    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @PostMapping("/setUserRole")
    @Transactional
    @ApiOperation(value="添加用户关联角色")
    public Result<Object> setUserRole(@RequestBody UserRoleDto userRoleDto) {
        //先删除当前用户下的所有角色
        userRoleMapper.deleteByUserId(userRoleDto.getUserId());
        //保存
        for (Integer roleId:userRoleDto.getRoleIds()) {
            UserRole userRole = new UserRole();
            userRole.setUserId(userRoleDto.getUserId());
            userRole.setRoleId(roleId);
            userRoleMapper.insert(userRole);
        }
        return Result.ok();
    }


    //新增或修改
    @Log(title = "角色管理", businessType = BusinessType.UPDATE)
    @PostMapping
    @ApiOperation(value="新增和修改")
    public Result<Object> saveOrUpdate(@RequestBody Role role) {
        return Result.ok(roleService.saveOrUpdate(role));
    }

    //查询所有数据
    @GetMapping
    @ApiOperation(value="查询所有数据")
    public Result<List<Role>> findAllRole() {
        return Result.ok(roleService.list());
    }

    //根据id查询
    @GetMapping("/{id}")
    @ApiOperation(value="根据id查询")
    public Result<Role> findOne(@PathVariable Integer id) {
        return Result.ok(roleService.getById(id));
    }

    //分页查询
    @GetMapping("/page")
    @ApiOperation(value="分页查询")
    public Result<IPage<Role>> selectPage(@RequestParam Integer pageNum,
                                          @RequestParam Integer pageSize,
                                          @RequestParam(required = false) String roleName) {
        IPage<Role> rolePage = new Page<>(pageNum, pageSize);
        QueryWrapper<Role> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(roleName)){
            queryWrapper.like("role_name", roleName);
        }
        queryWrapper.orderByDesc("nm_order");
        return Result.ok(roleService.page(rolePage, queryWrapper));
    }

    //根据id删除
    @Log(title = "角色管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{id}")
    @ApiOperation(value="根据id删除")
    public Result<Object> deleteByIdRole(@PathVariable("id") Integer id) {
        return Result.ok(roleService.removeById(id));
    }

    //批量删除
    @Log(title = "角色管理", businessType = BusinessType.DELETE)
    @PostMapping("/batchDelete")
    @ApiOperation(value="批量删除用户")
    public Result<Object> deleteBatchRole(@RequestBody List<Integer> ids) {
        return Result.ok(roleService.removeByIds(ids));
    }
}

