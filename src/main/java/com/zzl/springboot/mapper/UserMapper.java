package com.zzl.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zzl.springboot.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Zzl
 * @since 2024-03-17
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

}
