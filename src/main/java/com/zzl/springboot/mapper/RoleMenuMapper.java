package com.zzl.springboot.mapper;

import com.zzl.springboot.entity.Menu;
import com.zzl.springboot.entity.Role;
import com.zzl.springboot.entity.RoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Zzl
 * @since 2024-04-02
 */
@Mapper
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {

    @Select("select menu_id from sys_role_menu where role_id = #{roleId}")
    List<Integer> selectMenusByRoleId(Integer roleId);

    @Delete("delete from sys_role_menu where role_id = #{roleId}")
    void deleteByRoleId(Integer roleId);
}
