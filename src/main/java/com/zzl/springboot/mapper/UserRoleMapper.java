package com.zzl.springboot.mapper;

import com.zzl.springboot.entity.Role;
import com.zzl.springboot.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Zzl
 * @since 2024-04-02
 */
@Mapper
public interface UserRoleMapper extends BaseMapper<UserRole> {

    List<Role> selectRolesByUserId(Integer userId);

    @Delete("delete from sys_user_role where user_id = #{userId}")
    void deleteByUserId(Integer userId);

}
