package com.zzl.springboot.mapper;

import com.zzl.springboot.entity.MavTotal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Zzl
 * @since 2024-04-19
 */
@Mapper
public interface MavTotalMapper extends BaseMapper<MavTotal> {

}
