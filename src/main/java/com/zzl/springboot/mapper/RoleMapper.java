package com.zzl.springboot.mapper;

import com.zzl.springboot.entity.Menu;
import com.zzl.springboot.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Zzl
 * @since 2024-03-31
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {


}
