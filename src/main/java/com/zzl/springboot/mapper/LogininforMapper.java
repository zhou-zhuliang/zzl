package com.zzl.springboot.mapper;

import com.zzl.springboot.entity.Logininfor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Zzl
 * @since 2024-04-17
 */
@Mapper
public interface LogininforMapper extends BaseMapper<Logininfor> {

}
