package com.zzl.springboot.exception;

import com.zzl.springboot.common.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;


@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(ServiceException.class)
    @ResponseBody
    public Result<Object> handle(ServiceException serviceException) {
        log.error(serviceException.getLocalizedMessage());
        serviceException.printStackTrace();
        return Result.fail(serviceException.getCode(),serviceException.getMessage());

    }
}
