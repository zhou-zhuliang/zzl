package com.zzl.springboot.exception;

import com.zzl.springboot.common.Constants;
import lombok.Getter;

/**
 * 自定义异常
 */
@Getter
public class ServiceException extends RuntimeException {
    private final int code;

    public ServiceException(int code, String msg) {
        super(msg);
        this.code = code;
    }

    public ServiceException(String msg) {
        super(msg);
        this.code = Constants.CODE_600;
    }

}
