import Vue from 'vue'
import VueRouter from 'vue-router'
import store from "@/store/store";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: () => import('../views/Manage.vue'),
    redirect:"/login",
    children: [
      {path: '/home', name: '主页', component: () => import('../views/Home.vue')},
      {path: '/chart', name: '图表统计', component: () => import('../views/Chart.vue')},
      {path: '/user', name: '用户管理', component: () => import('../views/User.vue')},
      {path: '/person', name: '个人信息', component: () => import('../views/Person.vue')},
      {path: '/files', name: '文件管理', component: () => import('../views/Files.vue')},
      {path: '/role', name: '角色管理', component: () => import('../views/Role.vue')},
      {path: '/menu', name: '菜单管理', component: () => import('../views/Menu.vue')},
      {path: '/map', name: '高德地图', component: () => import('../views/Map.vue')},
      {path: '/photo', name: '照片墙', component: () => import('../views/Photo.vue')},
      {path: '/woodenfish', name: '木鱼', component: () => import('../views/Woodenfish.vue')},
      {path: '/video', name: '视频', component: () => import('../views/Video.vue')},
      {path: '/videoDetail', name: '视频播放', component: () => import('../views/VideoDetail.vue')},
      {path: '/loginInfo', name: '登录日志', component: () => import('../views/LoginInfo.vue')},
      {path: '/money', name: '我是秦始皇', component: () => import('../views/Money.vue')},
      {path: '/operLog', name: '操作日志', component: () => import('../views/OperLog.vue')},
    ]
  },
  {
    path: "/login",
    name: "Login",
    component: () => import('../views/Login.vue')
  },
  {
    path: "/register",
    name: "Register",
    component: () => import('../views/Register.vue')
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

//路由守卫
router.beforeEach((to,from,next) =>{
  localStorage.setItem("currentPathName",to.name) //设置当前路由名称，为了在header组件中使用
  store.commit("setPath") //触发store的数据更新
  next() //放行路由
})

export default router
